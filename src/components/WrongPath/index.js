import React from "react";
import {Redirect} from 'react-router-dom';

class WrongPath extends React.Component {
	state = {
		counter: 10,
	};
	componentDidMount = async () => {
		const intervalId = setInterval(this.countdown, 1000);
		this.setState({intervalId});
	};

	componentWillUnmount = () => {
		clearInterval(this.state.intervalId);
	};

	countdown = () => {
		this.setState({counter: this.state.counter - 1});
	};

	render() {
		const {location} = this.props;
		const {counter} = this.state;
		return (
			<div className={'Index'}>
				<p>404 Wrong Path. No match for <code>{location.pathname}</code></p>
				<p>Redirecting to homepage in {this.state.counter}</p>
				{counter === 0 && <Redirect to='/'/>}
			</div>
		);
	}
}

export default WrongPath;
