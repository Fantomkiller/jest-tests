import React from "react";
import {get, update} from '../../helpers/toDoItemApi';
import {Formik, Form } from 'formik';
import {
	SubmitButton,
	TextInput,
	Label,
	Select,
	ErrorMsg,
	InfoBox
} from '../../helpers/theme';
import * as Yup from 'yup';

class ToDoEditForm extends React.Component {
	state = {
		task: null,
		fetched: false,
	};
	itemId = () => this.props.match.params.itemId;

	componentDidMount = async () => {
		const task = await get(this.itemId());
		this.setState({task, fetched: true});
	};


	render() {
		const {fetched, task} = this.state;
		const formSchema = Yup.object().shape({
			id: Yup.string()
				.required('Required'),
			text: Yup.string()
				.min(4, 'Too Short!')
				.required('Required'),
			done: Yup.boolean()
				.required('Required')
		})
		return (
			<div className={'ToDoEditForm'}>
				Edit Form for {this.itemId()}
				{fetched
					? <Formik
						initialValues={{...task}}
						onSubmit={async (values) => {
							await update(this.itemId(), {...values});
						}}
						validationSchema={formSchema}
					>
						{({
							values,
							errors,
							touched,
							handleBlur,
							handleSubmit,
							isSubmitting,
							handleChange,
							validateForm
						}) => (
							<Form onSubmit={handleSubmit}>
								<Label>
									<InfoBox>{`Text *`}{errors.text && touched.text && <ErrorMsg>{errors.text}</ErrorMsg>}
									</InfoBox>
									<TextInput
										name={'text'}
										onChange={handleChange}
										value={values.text}
									/>
								</Label>
								<SubmitButton type={"submit"} onClick={()=> validateForm()}>
									Update
								</SubmitButton>
							</Form>
						)
						}
					</Formik>
					: <p>Loading ...</p>
				}
			</div>
		);
	}
}

export default ToDoEditForm;
