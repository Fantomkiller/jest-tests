import React from "react";
import NewTodoForm from "../_reusable/NewTodoForm";
import ToDoTask from "../_reusable/ToDoTask";
import styled from 'styled-components';
import * as toDoItemApi from '../../helpers/toDoItemApi';
import * as _ from 'ramda';

const Header = styled.h1`
	color: white;
`;

class ToDoList extends React.Component {
	componentDidMount = async () => {
		const tasks = await toDoItemApi.getAll();
		this.setState({tasks: tasks},
		);
	};

	static defaultProps = {
		title: 'My stuff',
		tasks: [],
	};
	state = {
		tasks: this.props.tasks,
		draft: '',
	};
	updateDraft = (event) => {
		this.setState({draft: event.target.value});
	};
	add = async () => {
		const {tasks, draft} = this.state;
		if (!draft) {
			return null;
		}
		await toDoItemApi.create({id: (tasks.length).toString(), text: draft, done: false});
		this.setState({draft: ''});
		this.setState({tasks: _.append({id: (tasks.length).toString(), text: draft, done: false}, tasks)});
	};

	findById = (id, arr) => {
		const index = _.findIndex(_.propEq('id', id))(arr);

		return {index, task: arr[index]};
	};

	deleteFromApi = async (id) => {
		const {tasks} = this.state;
		await toDoItemApi.destroy(id);
		const {index} = this.findById(id, tasks);
		this.setState({tasks: _.remove(index, 1, tasks)});
	};


	render() {
		const {title} = this.props;
		const {tasks, draft} = this.state;

		return (
			<div className={'Index'}>
				<Header>{title}</Header>
				{tasks.map(task =>
					<ToDoTask
						description={task.text}
						done={task.done}
						key={task.text + Math.random()}
						id={task.id}
						deleteFunction={this.deleteFromApi}
					/>)}
				<NewTodoForm
					onSubmitHandler={this.add}
					onChangeHandler={this.updateDraft}
					draft={draft}
					disabled={!draft}
				/>
			</div>
		);
	}
}

export default ToDoList;
