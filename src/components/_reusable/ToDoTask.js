import React from "react";
import styled from 'styled-components';
import * as toDoItemApi from '../../helpers/toDoItemApi';
import {Link} from 'react-router-dom'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
	faWrench,
} from '@fortawesome/free-solid-svg-icons'
library.add(
	faWrench
)
const Item = styled.div`
	cursor:pointer;
	background:#343744;
	border-radius: 14px 0px 0px 14px;
	display:flex;
	justify-content: space-between;
	flex-grow:1;
	margin-bottom:7px;
	margin-right:5px
	color: ${props => props.done ? '#bada55' : 'auto'};
	text-decoration: ${props => props.done ? 'line-through' : 'auto'};
	align-items:center;
`;
const Text = styled.div`
	padding:14px;
	flex-grow:1;
`

const Button = styled.button`
	cursor:pointer;
	background:#313440;
	border-radius: 0px 14px 14px 0px;
	padding:14px;
	margin-bottom:7px;
	display:flex;
	justify-content:center;
	align-content:center
	transition:all ease .2s
	font-weight:bold;
	color:#B23B00;
	border:none;
	&:hover {
		color:white;
		transform: translateY(-2px);
		background:#BC2E29;
	}
`

const Container = styled.div`
	display:flex
	justify-content:flex-start
`

const StyledLink = styled(Link)`
	opacity:50%;
	color: #00a7fa;
	padding:14px;
	&:hover {
	opacity:100%
	}
`

class ToDoTask extends React.Component {
	static defaultProps = {
		done: false,
	};
	state = {
		done: this.props.done,
	};
	toggleDone = async () => {
		toDoItemApi.update(this.props.id, {
			done: !this.state.done,
		})
		this.setState(this.setState({done: !this.state.done}));
	};
	destroy = () => {
		this.props.deleteFunction(this.props.id)
	}
	render() {
		const {description, id} = this.props;
		return (
			<Container>
			<Item
				done={this.state.done}
			>
				<Text onClick={this.toggleDone}>{description}</Text>
				<StyledLink to={`/task/${id}`}>
					<FontAwesomeIcon icon={faWrench} />
				</StyledLink>
			</Item>
				<Button onClick={this.destroy}>X</Button>
			</Container>
		);
	}
}

export default ToDoTask;
