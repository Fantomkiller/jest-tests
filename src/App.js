import React from 'react';
import './App.scss';
import ToDoEditForm from "./components/ToDoEditForm/index";
import ToDoList from "./components/ToDoList/index";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import styled from 'styled-components';
import WrongPath from "./components/WrongPath";

const Container = styled.div`
	background:#2b2e39;
	margin: 0 auto;
	width:80%;
	max-width:600px;
	padding:14px;
	border-radius:14px;
	margin-top:14px;
`;


function App() {

	return <Router>
		<Container>
			<Switch>
				<Route exact path={'/'} component={ToDoList}/>
				<Route exact path={'/task/:itemId'} component={ToDoEditForm}/>
				<Route component={WrongPath}/>
			</Switch>
		</Container>
	</Router>;
}

export default App;
