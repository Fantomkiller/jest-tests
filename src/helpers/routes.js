const serverUrl = 'http://localhost:3069';

export const toDoItemsApiUrl = id =>
	id ? `${serverUrl}/tasks/${id}` : `${serverUrl}/tasks/`;


